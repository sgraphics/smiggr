﻿using System;

namespace Smiggr.Core
{
	public abstract class BaseCommand
	{
		public abstract void Execute();
	}

	public abstract class BaseCommand<T> : BaseCommand
	{
		public T Result { get; protected set; }
	}

	/// <summary>
	/// Returns probable request length
	/// </summary>
	public class RequestLengthCommand : BaseCommand<double>
	{
		public override void Execute()
		{
			Result = Math.Pow(1.011, (UserCount + 10)) * 0.2 + 0.2;
		}

		public int UserCount { get; set; }
	}
}
