using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Smiggr.Core.Algorithm;
using System.Linq;
using Smiggr.Core.TimeServices;

namespace Smiggr.Core.StepExecutors
{
	public interface IStepExecutor
	{
		void Start();
		Task ExecuteAsync(AdditionStep step);
		Task ExecuteAsync(SubsitutionStep step);
		Task ExecuteAsync(SubtractionStep step);
	}

	public class SimulatorStepExecutor : AmazonStepExecutor
	{
		public FixedSizedQueue<string> CallLog { get; set; }
		public SimulatorTimeService SimulatorTimeService
		{
			get { return TimeService as SimulatorTimeService; }
		}

		public SimulatorStepExecutor()
		{
			ResetCallLog();
		}

		public void ResetCallLog()
		{
			CallLog = new FixedSizedQueue<string>
			{
				Limit = 30
			};
		}

		protected override void CopySnapshot()
		{
			CallLog.Limit = 30;
			CallLog.Enqueue(SimulatorTimeService.CurrentTime.ToShortTimeString() + ": CopySnapshot()");
			Thread.Sleep(SimulatorTimeService.GetMilliseconds(180));
		}

		protected override void RegisterImage()
		{
			CallLog.Limit = 30;
			CallLog.Enqueue(SimulatorTimeService.CurrentTime.ToShortTimeString() + ": RegisterImage()");
			Thread.Sleep(SimulatorTimeService.GetMilliseconds(30));
		}

		protected override void RunInstance()
		{
			CallLog.Limit = 30;
			CallLog.Enqueue(SimulatorTimeService.CurrentTime.ToShortTimeString() + ": RunInstance()");
			Thread.Sleep(SimulatorTimeService.GetMilliseconds(300));
		}

		protected override void StopInstance()
		{
			CallLog.Limit = 30;
			CallLog.Enqueue(SimulatorTimeService.CurrentTime.ToShortTimeString() + ": StopInstance()");
			Thread.Sleep(SimulatorTimeService.GetMilliseconds(60));
		}
	}

	public class FixedSizedQueue<T> : Queue<T>
	{
		public int Limit { get; set; }

		public FixedSizedQueue()
		{
			
		}

		public FixedSizedQueue(int limit)
		{
			Limit = limit;
		}

		public new void Enqueue(T item)
		{
			base.Enqueue(item);
			lock (this)
			{
				while (Count > Limit)
					Dequeue();
			}
		}
	}

	public abstract class AmazonStepExecutor : IStepExecutor
	{
		public IRepository Repository { get; set; }
		public ITimeService TimeService { get; set; }
		private Timer Timer { get; set; }

		private void ExecuteAll(object state)
		{
			//((SimulatorStepExecutor)this).CallLog.Enqueue((TimeService as SimulatorTimeService).CurrentTime.ToShortTimeString() + ": Execute()");
			var services = Repository.GetServiceDefinitions().ToList().Where(x => x.StepsToExecute.ToList().Any());

			foreach (var service in services)
			{
				var step = service.StepsToExecute.Peek();

				if (step == null || step.State == StepState.Executing)
					continue;

				if (step.State == StepState.Executed)
				{
					service.StepsToExecute.Dequeue();
					continue;
				}

				step.State = StepState.Executing;

				if (step is AdditionStep)
					ExecuteAsync(step as AdditionStep);

				if (step is SubsitutionStep)
					ExecuteAsync(step as SubsitutionStep);

				if (step is SubtractionStep)
					ExecuteAsync(step as SubtractionStep);
			}
		}

		public void Start()
		{
			Timer = new Timer(ExecuteAll, null, 50, 50);
		}

		public async Task ExecuteAsync(AdditionStep step)
		{
			RegisterImage();
			RunInstance();
			step.DataCenter.VirtualMachines.Add(new VirtualMachine
			{
				Id = Guid.NewGuid().ToString(),
				ServiceDefinitionId = step.Service.Id
			});
			step.State = StepState.Executed;
		}

		public async Task ExecuteAsync(SubsitutionStep step)
		{
			RegisterImage();
			RunInstance();
			StopInstance();
			var virtualMachine = step.DataCenter.VirtualMachines.FirstOrDefault(x => x.Id == step.VirtualMachineId);
			virtualMachine.Id = Guid.NewGuid().ToString();

			step.State = StepState.Executed;
		}

		public async Task ExecuteAsync(SubtractionStep step)
		{
			StopInstance();
			var virtualMachine = step.DataCenter.VirtualMachines.FirstOrDefault(x => x.Id == step.VirtualMachineId);
			step.DataCenter.VirtualMachines.Remove(virtualMachine);

			step.State = StepState.Executed;
		}

		protected virtual void CopySnapshot()
		{
			//TODO
		}

		protected virtual void RegisterImage()
		{
			//TODO
		}

		protected virtual void RunInstance()
		{
			//TODO
		}

		protected virtual void StopInstance()
		{
			//TODO
		}
	}
}