using System;

namespace Smiggr.Core.TimeServices
{
	public interface ITimeService
	{
		DateTime GetNewTimeValue();
	}

	public class SimulatorTimeService : ITimeService
	{
		public DateTime CurrentTime { get; set; }
		private int TimeStep { get; set; }

		public bool IsAtEnd()
		{
			return CurrentTime >= GetEndTime();
		}

		public int GetPercentageDone()
		{
			double totalMinutes = (GetEndTime() - GetStartTime()).TotalMinutes;
			double passedMinutes = (CurrentTime - GetStartTime()).TotalMinutes;

			return Convert.ToInt32(passedMinutes*100/totalMinutes);
		}

		public void Reset()
		{
			CurrentTime = DateTime.MinValue;
		}

		private DateTime GetEndTime()
		{
			return DateTime.MinValue.AddHours(8);
		}

		private DateTime GetStartTime()
		{
			return DateTime.MinValue;
		}

		public SimulatorTimeService()
		{
			TimeStep = 5;
		}

		/// <summary>
		/// Advances the time and returns the new value
		/// </summary>
		public DateTime GetNewTimeValue()
		{
			CurrentTime = CurrentTime.AddMinutes(TimeStep);
			return CurrentTime;
		}

		public static int GetMilliseconds(int seconds)
		{
			// 60sec = 50ms;
			return Convert.ToInt32(seconds/60*50);
		}
	}

	public class TimeService : ITimeService
	{
		public DateTime GetNewTimeValue()
		{
			return DateTime.Now;
		}
	}
}