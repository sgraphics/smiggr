using System;
using System.Collections.Generic;
using System.Linq;
using Smiggr.Core.StepExecutors;

namespace Smiggr.Core.Algorithm
{
	public class WorkerUpdate
	{
		public DateTime Time { get; set; }
	}

	public class VirtualMachineMeasurements
	{
		public string VirtualMachineId { get; set; }

		/// <summary>
		/// Unit id and queue of measurements
		/// </summary>
		public IDictionary<string, FixedSizedQueue<double>> Measurements { get; set; }

		public VirtualMachineMeasurements()
		{
			Measurements = new Dictionary<string, FixedSizedQueue<double>>();
		}
	}

	public class Unit
	{
		public string Id { get; set; }
	}

	public interface IRepository
	{
		IList<DataCenter> GetDatacenters();
		IList<ServiceDefinition> GetServiceDefinitions();
		IList<Unit> GetUnits();
		double GetCriticalLoad();
		double GetMinLoad();
		double GetMaxLoad();
	}

	public class SimulatorRepository : IRepository
	{
		public static int MaxLoad { get; set; }

		public static int MinLoad { get; set; }

		public static int MaxRequestsPerMachine = 100;

		private IList<DataCenter> DataCenters { get; set; }
		private IList<ServiceDefinition> ServiceDefinitions { get; set; }

		/// <summary>
		/// datacenter id, hour, reqs per second
		/// </summary>
		public static Dictionary<string, Dictionary<int, double>> DataCenterLoads { get; set; }

		public double[] Steps = new[] { .02, .05, 1, 3, 5, 10, 30, 50, 70, 100, 120, 150, 200, 250, 300, 500 };

		public IList<ServiceDefinition> GetServiceDefinitions()
		{
			ServiceDefinitions = ServiceDefinitions ?? new List<ServiceDefinition>
			{
				new ServiceDefinition
				{
					Id = Guid.NewGuid().ToString()
				},
				new ServiceDefinition
				{
					Id = Guid.NewGuid().ToString()
				}
			};

			return ServiceDefinitions;
		}

		public IList<DataCenter> GetDatacenters()
		{
			DataCenters = DataCenters ?? new List<DataCenter>
			{
				new DataCenter
				{
					Id = Guid.NewGuid().ToString(),
					VirtualMachines = new List<VirtualMachine>
					{
						new VirtualMachine
						{
							Id = Guid.NewGuid().ToString(),
							ServiceDefinitionId = GetServiceDefinitions().FirstOrDefault().Id
						},
						new VirtualMachine
						{
							Id = Guid.NewGuid().ToString(),
							ServiceDefinitionId = GetServiceDefinitions().FirstOrDefault().Id
						},
						new VirtualMachine
						{
							Id = Guid.NewGuid().ToString(),
							ServiceDefinitionId = GetServiceDefinitions().FirstOrDefault().Id
						},
					}
				},
				//new DataCenter
				//{
				//	Id = Guid.NewGuid().ToString(),
				//	VirtualMachines = new List<VirtualMachine>
				//	{
				//		new VirtualMachine
				//		{
				//			Id = Guid.NewGuid().ToString(),
				//			ServiceDefinitionId = GetServiceDefinitions().Skip(1).FirstOrDefault().Id
				//		},
				//		new VirtualMachine
				//		{
				//			Id = Guid.NewGuid().ToString(),
				//			ServiceDefinitionId = GetServiceDefinitions().Skip(1).FirstOrDefault().Id
				//		},
				//		new VirtualMachine
				//		{
				//			Id = Guid.NewGuid().ToString(),
				//			ServiceDefinitionId = GetServiceDefinitions().Skip(1).FirstOrDefault().Id
				//		},
				//	}
				//},
			};
			return DataCenters;
		}

		public IList<Unit> GetUnits()
		{
			return new List<Unit>
			{
				new Unit { Id = "CPU" },
				new Unit { Id = "Memory" },
				new Unit { Id = "Bandwidth" }
			};
		}

		public double GetMinLoad()
		{
			return MinLoad;
		}

		public double GetMaxLoad()
		{
			return MaxLoad;
		}

		public double GetCriticalLoad()
		{
			return 90;
		}
	}
}