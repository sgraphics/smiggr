namespace Smiggr.Core.Algorithm
{
	public enum WorkerState
	{
		Started,
		Starting,
		Stopping,
		Stopped,
		Reset
	}
}