﻿using System;
using System.Collections.Generic;
using System.Threading;
using Smiggr.Core.DataCollectors;
using Smiggr.Core.StepExecutors;
using Smiggr.Core.TimeServices;
using System.Linq;

namespace Smiggr.Core.Algorithm
{
    public class Worker
    {
        private readonly static object _stateChangeLock = new object();
        private readonly int _updateInterval = 250; //ms
        private Timer _timer;
        private readonly object _runAlgorithmOnce = new object();
        private bool _runningAlgorithm;
        private WorkerState _workerState = WorkerState.Stopped;
	    public Action<WorkerUpdate> WorkerUpdate { get; set; }
	    public Func<bool> StopCondition { get; set; }

		public ITimeService TimeService { get; set; }
		public IDataCollector DataCollector { get; set; }
		public IStepExecutor StepExecutor { get; set; }
		public IRepository Repository { get; set; }

        public WorkerState WorkerState
        {
            get { return _workerState; }
            private set { _workerState = value; }
        }

		private void RunAlgorithm(object state)
		{
			//Ensure no double invocations
			if (_runningAlgorithm)
				return;

			lock (_runAlgorithmOnce)
			{
				if (!_runningAlgorithm)
				{
					_runningAlgorithm = true;

					if (StopCondition != null && StopCondition())
						Stop();

					var currentTime = TimeService.GetNewTimeValue();
					var dataCenters = Repository.GetDatacenters();
					var minLoad = Repository.GetMinLoad();
					var maxLoad = Repository.GetMaxLoad();
					var criticalLoad = Repository.GetCriticalLoad();
					var services = Repository.GetServiceDefinitions().Where(x => !x.StepsToExecute.Any()).ToList();

					//Get steps for data center, only if there isnt any pending steps
					foreach (var dataCenter in dataCenters)
					{
						var datacenterMetrics = DataCollector.GetMetrics(dataCenter).ToList();

						//Take only those services from each datacenter that do not have any pending changes
						foreach (var virtualMachinesByService in dataCenter.VirtualMachines.Where(vm => services.Any(s => s.Id == vm.ServiceDefinitionId)).GroupBy(x => x.ServiceDefinitionId))
						{
							var service = services.FirstOrDefault(x => x.Id == virtualMachinesByService.Key);
							var metrics = datacenterMetrics.Where(x => virtualMachinesByService.Any(vm => vm.Id == x.VirtualMachineId)).ToList();

							//Add new machines if average value of a measurement unit is above maxload
							if (Repository.GetUnits().Any(unit =>
								{
									var averageUnitLoad = metrics.Average(metric => metric.Measurements.Where(x => x.Key == unit.Id).SelectMany(x => x.Value).Average());
									return averageUnitLoad > maxLoad;
								}))
								service.StepsToExecute.Enqueue(new AdditionStep(dataCenter, service));

							if (service.StepsToExecute.Any())
								break;

							//Check if any machine needs to be substituted for faulty behaviour
							foreach (var metric in metrics.Where(metric => metric.Measurements.Values.Any(measurement => measurement.Average() > criticalLoad)))
								service.StepsToExecute.Enqueue(new SubsitutionStep(dataCenter, service, metric.VirtualMachineId));

							if (service.StepsToExecute.Any())
								break;

							//Delete the slowest machine when overall under minimum load
							double maxAverage = Repository.GetUnits().Max(unit => metrics.Average(vm => vm.Measurements.Where(m => m.Key == unit.Id).SelectMany(m => m.Value).Average()));
							if (metrics.Count > 2 && maxAverage < minLoad)
								service.StepsToExecute.Enqueue(new SubtractionStep(dataCenter, service, metrics.OrderBy(x => x.Measurements.Values.Max(m => m.Average())).FirstOrDefault().VirtualMachineId));
						}
					}

					WorkerUpdate(new WorkerUpdate
					{
						Time = currentTime,
					});

					_runningAlgorithm = false;
				}
			}
		}

	    public void Start()
        {
            if (WorkerState != WorkerState.Started && WorkerState != WorkerState.Starting)
            {
                lock (_stateChangeLock)
                {
                    if (WorkerState != WorkerState.Started && WorkerState != WorkerState.Starting)
                    {
                        WorkerState = WorkerState.Starting;
                        _timer = new Timer(RunAlgorithm, null, _updateInterval, _updateInterval);
                        WorkerState = WorkerState.Started;
                    }
                }
            }
        }

        public void Stop()
        {
            if (WorkerState == WorkerState.Started || WorkerState == WorkerState.Starting)
            {
                lock (_stateChangeLock)
                {
                    if (WorkerState == WorkerState.Started || WorkerState == WorkerState.Starting)
                    {
                        WorkerState = WorkerState.Stopping;
                        if (_timer != null)
                        {
                            _timer.Dispose();
                        }
                        WorkerState = WorkerState.Stopped;
                    }
                }
            }
        }

        public void Reset()
        {
            lock (_stateChangeLock)
            {
                if (WorkerState != WorkerState.Stopped)
                {
                    throw new InvalidOperationException("Market must be stopped before it can be reset.");
                }
	            WorkerState = WorkerState.Reset;
            }
        }
    }

	public class SubtractionStep : Step
	{
		public string VirtualMachineId { get; set; }

		public SubtractionStep(DataCenter virtualMachine, ServiceDefinition service, string virtualMachineId) : base(virtualMachine, service)
		{
			VirtualMachineId = virtualMachineId;
		}
	}

	public class AdditionStep : Step
	{
		public AdditionStep(DataCenter virtualMachine, ServiceDefinition service) : base(virtualMachine, service)
		{
		}
	}

	public class SubsitutionStep : Step
	{
		public string VirtualMachineId { get; set; }

		public SubsitutionStep(DataCenter virtualMachine, ServiceDefinition service, string virtualMachineId) : base(virtualMachine, service)
		{
			VirtualMachineId = virtualMachineId;
		}
	}

	public enum StepState
	{
		NotExecuted,
		Executing,
		Executed
	}

	public abstract class Step
	{
		public DataCenter DataCenter { get; set; }
		public ServiceDefinition Service { get; set; }
		public StepState State { get; set; }

		public Step(DataCenter dataCenter, ServiceDefinition service)
		{
			DataCenter = dataCenter;
			Service = service;
		}
	}

	public class DataCenter
	{
		public string Id { get; set; }
		public IList<VirtualMachine> VirtualMachines { get; set; }

		public DataCenter()
		{
			VirtualMachines = new List<VirtualMachine>();
		}
	}

	public class ServiceDefinition
	{
		public string Id { get; set; }
		public Queue<Step> StepsToExecute { get; set; }

		public ServiceDefinition()
		{
			StepsToExecute = new Queue<Step>();
		}
	}

	public class VirtualMachine
	{
		public string Id { get; set; }
		public string ServiceDefinitionId { get; set; }
	}
}