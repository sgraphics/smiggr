﻿using System;
using System.Collections.Generic;
using Smiggr.Core.Algorithm;
using Smiggr.Core.StepExecutors;
using Smiggr.Core.TimeServices;
using System.Linq;

namespace Smiggr.Core.DataCollectors
{
	public interface IDataCollector
	{
		IList<VirtualMachineMeasurements> GetMetrics(DataCenter dataCenter);
	}

	public class SimulatorDataCollector : IDataCollector
	{
		private IList<VirtualMachineMeasurements> Measurements { get; set; }
		public ITimeService TimeService { get; set; }
		public IRepository Repository { get; set; }

		public IList<VirtualMachineMeasurements> GetMetrics(DataCenter dataCenter)
		{
			Measurements = Measurements ?? new List<VirtualMachineMeasurements>();
			var timeService = TimeService as SimulatorTimeService;
			var requestsPerSecond = GetRequestsPerSecond(dataCenter, timeService.CurrentTime);

			var load = GetLoad(dataCenter, requestsPerSecond);

			foreach (var vm in dataCenter.VirtualMachines)
			{
				var measurement = Measurements.FirstOrDefault(x => x.VirtualMachineId == vm.Id);
				if (measurement == null)
				{
					measurement = new VirtualMachineMeasurements
					{
						VirtualMachineId = vm.Id
					};
					Measurements.Add(measurement);
				}

				foreach (var unit in Repository.GetUnits())
				{
					if (!measurement.Measurements.ContainsKey(unit.Id))
						measurement.Measurements[unit.Id] = new FixedSizedQueue<double>(3);

					measurement.Measurements[unit.Id].Enqueue(load);
				}
			}

			return Measurements;
		}

		public double GetLoad(DataCenter dataCenter, double requestsPerSecond)
		{
			return requestsPerSecond/dataCenter.VirtualMachines.Count*100/SimulatorRepository.MaxRequestsPerMachine;
		}

		public double GetRequestsPerSecond(DataCenter dataCenter, DateTime time)
		{
			var previousRPS = SimulatorRepository.DataCenterLoads[dataCenter.Id][time.Hour];
			var nextRPS = SimulatorRepository.DataCenterLoads[dataCenter.Id][time.Hour + 1];
			var requestsPerSecond = previousRPS + ((nextRPS - previousRPS) * (time.Minute / 60D));
			return requestsPerSecond;
		}
	}
}