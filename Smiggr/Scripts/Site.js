﻿/// <reference path="../scripts/jquery-1.8.3.js" />
/// <reference path="../scripts/jquery.signalR-1.0.0.js" />

/*!
    ASP.NET SignalR Stock Ticker Sample
*/

// Crockford's supplant method (poor man's templating)
if (!String.prototype.supplant) {
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }
        );
    };
}

$.fn.serializeObject = function () {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function () {
		var name = this.name.replace(/[\.\[\]]/g, "_");
		if (o[name] !== undefined) {
			if (!o[name].push) {
				o[name] = [o[name]];
			}
			o[name].push(this.value || '');
		} else {
			o[name] = this.value || '';
		}
	});
	return o;
};

$(function () {

	var worker = $.connection.simulatorWorker, // the generated client-side hub proxy
		console = $('#console'),
		simulator = $('#simulator'),
		simulatorHtml = $('#simulator').html(),
		status = $('#status tbody'),
		startButton = $('#start'),
		dcVm = [],
		dcRps = [],
		dcLoad = [],
		time = $('#time');

	dcVm[0] = [];
	dcRps[0] = [];
	dcLoad[0] = [];

	renderFlot();

    function reset() {
    	simulator.html(simulatorHtml);
    	worker = $.connection.simulatorWorker, // the generated client-side hub proxy
			console = $('#console'),
			simulator = $('#simulator'),
			simulatorHtml = $('#simulator').html(),
			status = $('#status tbody'),
			startButton = $('#start'),
			dcVm = [],
			dcRps = [],
			dcLoad = [],
			time = $('#time');

    	dcVm[0] = [];
    	dcRps[0] = [];
    	dcLoad[0] = [];

    	renderFlot();
    }

    // Add client-side hub methods that the server will call
    $.extend(worker.client, {
        metricsUpdate: function (metrics) {
        	//startButton.animate({ marginLeft: metrics.Percentage }, 1000, 'linear');
        	time.html(metrics.time);
	        //startButton.css("left", startButton.parent().width() * ((metrics.percentage) / 100) + "px");
        	startButton.animate({ left: (startButton.parent().width() - startButton.width()) * ((metrics.percentage) / 100) + "px" }, 200, 'linear');
        	console.html(metrics.callLog);
        	status.find("tr").remove();
	        var i = 0;
	        $.each(metrics.datacenters, function(index, datacenter) {
	        	status.append($('<tr id="' + index + '"><td>' + (index + 1) + '</td><td>' + datacenter.vmCount + '</td></tr>'));
	        	dcVm[i][dcVm[i].length] = [metrics.hour, datacenter.vmCount];
	        	dcLoad[i][dcLoad[i].length] = [metrics.hour, datacenter.load];
	        	dcRps[i][dcRps[i].length] = [metrics.hour, datacenter.rps];
		        i++;
	        });
	        renderFlot();
        },
        
		setAtEnd: function() {
		},

        reset: function () {
        	reset();
	        startConnection();
        }
    });

	startConnection();

	function startConnection() {
		// Start the connection
		$.connection.hub.start()
			.pipe(function () {
        		// Wire up the buttons
				$("#start").click(function () {
					/*
					worker.server.start($(".simulatorData form").serializeArray());
					worker.server.start($(".simulatorData form").serializeObject());
					worker.server.start($(".simulatorData form").serialize());

					
					worker.server.start({ data : $(".simulatorData form").serializeArray()});
					worker.server.start({ data : $(".simulatorData form").serializeObject()});
					worker.server.start({ data : $(".simulatorData form").serialize()});


					*/
					$.post(simulatorDataUrl, $(".simulatorData form").serialize(), function (data, status) {
						worker.server.start();
					});
        		});

        		$("#stop").click(function () {
        			worker.server.stop();
        		});

        		$("#reset").click(function () {
        			worker.server.reset();
        		});
			});
	}
	
	function renderFlot() {
		$.plot($('#placeholder'), [
			{
				data: dcVm[0],
				label: 'Region 1 VM count',
        		lines: { show: true },
        		points: { show: true }
			},
			{
				data: dcRps[0],
				label: 'Requests per second',
        		lines: { show: true },
        		points: { show: true },
				yaxis: 3
			},
			{
				data: dcLoad[0],
				label: 'Average load',
        		lines: { show: true },
        		points: { show: true },
				yaxis: 2
			}
		]);
	}
});