﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Smiggr.Core.Algorithm;
using Smiggr.Models;

namespace Smiggr.Controllers
{
    public class SimulatorController : Controller
    {
		public IRepository Repository { get; set; }
		public SimulatorRepository SimulatorRepository { get { return Repository as SimulatorRepository; } }
		public Random Random { get; set; }

        public ActionResult Index()
        {
			Random = new Random();
            return View(new SimulatorDataViewModel
            {
				Steps = SimulatorRepository.Steps,
				DataCenters = Repository.GetDatacenters().Select(x => new DataCenterViewModel
				{
					Id = x.Id,
					Loads = Enumerable.Range(0, 10).Select(l => new DataCenterLoadViewModel
					{
						Hour = l,
						Load = SimulatorRepository.Steps[Random.Next(5, 12)]
					}).ToList()
				}).ToList(),
            });
        }

		[HttpPost]
        public ActionResult Simulate()
        {
	        return RedirectToAction("Index");
        }

		[HttpPost]
		public ActionResult SetUpData(IList<DataCenterViewModel> dataCenters)
		{
			SimulatorRepository.DataCenterLoads = dataCenters.ToDictionary(x => x.Id, x => x.Loads.ToDictionary(l => l.Hour, l => l.Load));
			SimulatorRepository.MaxLoad = 60;
			SimulatorRepository.MinLoad = 20;
			return new EmptyResult();
	    }
    }
}
