﻿using System;
using System.Web.Mvc;
using Smiggr.Core;

namespace Smiggr.Controllers
{
	public class BaseController : Controller
	{
		public Action<BaseCommand> AlternativeExecuteCommand { get; set; }
		public Func<BaseCommand, object> AlternativeExecuteCommandWithResult { get; set; }

		public void ExecuteCommand(BaseCommand cmd)
		{
			if (AlternativeExecuteCommand != null)
				AlternativeExecuteCommand(cmd);
			else
				Default_ExecuteCommand(cmd);
		}

		public TResult ExecuteCommand<TResult>(BaseCommand<TResult> cmd)
		{
			if (AlternativeExecuteCommandWithResult != null)
				return (TResult)AlternativeExecuteCommandWithResult(cmd);
			return Default_ExecuteCommand(cmd);
		}

		protected void Default_ExecuteCommand(BaseCommand cmd)
		{
			cmd.Execute();
		}

		protected TResult Default_ExecuteCommand<TResult>(BaseCommand<TResult> cmd)
		{
			ExecuteCommand((BaseCommand)cmd);
			return cmd.Result;
		}
	}
}