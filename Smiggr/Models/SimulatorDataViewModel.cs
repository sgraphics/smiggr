﻿using System.Collections.Generic;

namespace Smiggr.Models
{
	public class SimulatorDataViewModel
	{
		public IList<DataCenterViewModel> DataCenters { get; set; }
		public double[] Steps { get; set; }
		public string Test { get; set; }
	}

	public class DataCenterViewModel
	{
		public string Id { get; set; }
		public IList<DataCenterLoadViewModel> Loads { get; set; }
	}

	public class DataCenterLoadViewModel
	{
		public int Hour { get; set; }
		public double Load { get; set; }
	}
}