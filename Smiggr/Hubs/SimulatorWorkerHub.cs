﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Smiggr.Core.Algorithm;
using Smiggr.Core.DataCollectors;
using Smiggr.Core.StepExecutors;
using Smiggr.Core.TimeServices;
using System.Linq;
using Smiggr.Models;

namespace Smiggr.Hubs
{
    [HubName("simulatorWorker")]
    public class SimulatorWorkerHub : Hub
    {
		private readonly Worker _worker;

		public SimulatorWorkerHub()
		{
			_worker = MvcApplication.Container.Resolve<Worker>();
			_worker.WorkerUpdate = OnWorkerUpdate;
			_worker.StopCondition = () => (MvcApplication.Container.Resolve<ITimeService>() as SimulatorTimeService).IsAtEnd();
		}

	    private void OnWorkerUpdate(WorkerUpdate e)
	    {
	        var timeService = MvcApplication.Container.Resolve<ITimeService>() as SimulatorTimeService;
	        var repository = MvcApplication.Container.Resolve<IRepository>() as SimulatorRepository;
			var dataCollector = MvcApplication.Container.Resolve<IDataCollector>() as SimulatorDataCollector;
			var stepExecutor = MvcApplication.Container.Resolve<IStepExecutor>() as SimulatorStepExecutor;

		    IList<DataCenter> dataCenters = repository.GetDatacenters().ToList();
		    Clients.All.metricsUpdate(new
		    {
			    time = e.Time.ToShortTimeString(),
				callLog = string.Join("<br/>", stepExecutor.CallLog.ToList().Take(14)),
				percentage = timeService.GetPercentageDone(),
				hour = e.Time.Hour * 10 + e.Time.Minute * 100 / 60 / 10,
				datacenters = dataCenters.Select(ds =>
					{
						var requestsPerSecond = dataCollector.GetRequestsPerSecond(ds, e.Time);
						return new
					    {
						    id = ds.Id,
						    rps = requestsPerSecond,
						    load = dataCollector.GetLoad(ds, requestsPerSecond),
						    vmCount = ds.VirtualMachines.Count,
					    };
					}).ToList()
		    });

		    if (timeService.IsAtEnd())
			    Clients.All.setAtEnd();
	    }

        public void Start()
		{
			_worker.Start();
        }

        public void Stop()
        {
            _worker.Stop();
        }

        public void Reset()
        {
            _worker.Reset();
	        var timeService = MvcApplication.Container.Resolve<ITimeService>() as SimulatorTimeService;
			timeService.Reset();
			var stepExecutor = MvcApplication.Container.Resolve<IStepExecutor>() as SimulatorStepExecutor;
			stepExecutor.ResetCallLog();
			Clients.All.reset();
        }
    }
}