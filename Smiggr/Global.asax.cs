﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Smiggr.Core.Algorithm;
using Smiggr.Core.StepExecutors;
using Smiggr.Mechanics;

namespace Smiggr
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{
		public static WindsorContainer Container { get; set; }

		protected void Application_Start()
		{
			//RouteTable.Routes.MapHubs();
			AreaRegistration.RegisterAllAreas();
			WebApiConfig.Register(GlobalConfiguration.Configuration);
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AuthConfig.RegisterAuth();

			Container = new WindsorContainer();

			Container.Register(Classes.FromThisAssembly().BasedOn<Controller>().LifestyleTransient());
			Container.Register(Classes.FromAssembly(typeof (Worker).Assembly).Pick().LifestyleSingleton().WithService.DefaultInterfaces());
			ControllerBuilder.Current.SetControllerFactory(new CastleControllerFactory(Container.Kernel));

			Container.Resolve<IStepExecutor>().Start();
		}

		protected void Application_BeginRequest(object src, EventArgs e)
		{
			Thread.CurrentThread.CurrentCulture = new CultureInfo("et-EE");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("et-EE");
		}
	}
}