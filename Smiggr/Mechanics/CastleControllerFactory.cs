﻿using System;
using System.Web;
using System.Web.Mvc;
using Castle.MicroKernel;

namespace Smiggr.Mechanics
{
	public class CastleControllerFactory : DefaultControllerFactory
	{
		private readonly IKernel kernel;

		public CastleControllerFactory(IKernel kernel)
		{
			this.kernel = kernel;
		}

		public override void ReleaseController(IController controller)
		{
			kernel.ReleaseComponent(controller);
		}

		protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
		{
			if (controllerType == null)
			{
				throw new HttpException(404, string.Format("The controller for path '{0}' could not be found.", requestContext.HttpContext.Request.Path));
			}
			return (IController)kernel.Resolve(controllerType);
		}
	}
}